import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, pipe } from 'rxjs';
import { take, map, mergeMap } from 'rxjs/operators';

@Injectable()
export class FilmesProvider {
  readonly urlBase:string = 'http://localhost:3000/';
  readonly urlListaDeFilmes = `${this.urlBase}filmes/lista`;
  readonly urlSalvarFilme = `${this.urlBase}filmes/novo`;
  readonly urlCategoriaAll = `${this.urlBase}filmes/categoria/all`;

  constructor(public http: HttpClient) {}

  getListaDeFilmesGroup():Observable<any[]>{
    return this.getCategorias().pipe(
      mergeMap(categorias =>{
        return this.http.get<any[]>(this.urlListaDeFilmes).pipe(
          map(filmes => {
            categorias.map((categoria:any) =>{
              categoria['filmes'] = filmes.filter((filme:any) => filme.categoria.id == categoria.id);
            });
            return categorias;
          })
        );
      }))
  }

  getListaDeFilmes(pesquisa, limit, page):Observable<any[]>{
        return this.http.get<any[]>(this.urlListaDeFilmes,{params:{like: pesquisa, limit, page}});
  }

  getCategorias():Observable<any[]>{
    return this.http.get<any[]>(this.urlCategoriaAll);
  }

  salvarFilme(filme):Observable<any>{
    let form = new FormData();
    form.append('nome', filme.nome);
    form.append('image', filme.image);
    form.append('categoriaId', filme.categoriaId);
    form.append('duracao', filme.duracao);

    return this.http.post(this.urlSalvarFilme, form);
  }
}
