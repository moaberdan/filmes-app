import { NgModule } from '@angular/core';
import { SlideFilmesComponent } from './slide-filmes/slide-filmes';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
	declarations: [SlideFilmesComponent],
	imports: [IonicPageModule],
	exports: [SlideFilmesComponent]
})
export class ComponentsModule {}
