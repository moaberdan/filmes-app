import { Component, OnInit, Input } from '@angular/core';
import {  Platform } from 'ionic-angular';
import { Observable } from 'rxjs';
@Component({
  selector: 'slide-filmes',
  templateUrl: 'slide-filmes.html'
})
export class SlideFilmesComponent implements OnInit {

  slidesPerView:number;

  @Input() filmes: Array<any> | Observable<any>;
  isArray:boolean;

  constructor(
    private platform: Platform) {

    this.slidesPerView = 5;

    // On a desktop, and is wider than 1200px
    if (this.platform.width() >= 1200) {
      this.slidesPerView = 5;
    }

    // On a desktop, and is wider than 768px
    else if (this.platform.width() >= 768) {
      this.slidesPerView = 4;
    }

    // On a desktop, and is wider than 400px
    else if (this.platform.width() >= 400) {
      this.slidesPerView = 2;
    }
    // On a desktop, and is wider than 319px
    else  {
      this.slidesPerView = 1;
    }
  }

  ngOnInit(){
    if(Array.isArray(this.filmes)){
      this.isArray = true;
    }else {
      this.isArray = false;
    }
  }
}
