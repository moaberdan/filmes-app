import { Component, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoadingController, NavController } from 'ionic-angular';
import { take } from 'rxjs/operators';

import { FilmesProvider } from '../../providers/filmes/filmes';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-novo-filme',
  templateUrl: 'novo-filme.html'
})
export class NovoFilmePage {
  filmeForm:FormGroup
  categorias$:Observable<any>;

  constructor(
    private loadCtrl:LoadingController,
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef,
    private filmesProvider: FilmesProvider) {

      this.filmeForm = formBuilder.group({
        nome: ['', Validators.required],
        image:[null, Validators.required],
        categoriaId:['', Validators.required],
        duracao:['', Validators.required],
      });
  }

  ionViewWillEnter(){
    this.categorias$ = this.filmesProvider.getCategorias().pipe(take(1));
  }

  salvar(){
   const loading = this.loadCtrl.create({ content: "Aguarde..." });
   loading.present();
   this.filmesProvider.salvarFilme(this.filmeForm.value).pipe(take(1)).subscribe(data =>{
      this.filmeForm.reset();
      loading.dismiss();
    });
  }

  onFileChange(event) {
      if(event._native.nativeElement.files && event._native.nativeElement.files.length) {
      const [file] = event._native.nativeElement.files;
      this.filmeForm.patchValue({
        image: file
      });
      this.cd.markForCheck();
    }
  }
}
