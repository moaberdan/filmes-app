import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';

import { FilmesProvider } from '../../providers/filmes/filmes';

@Component({
  selector: 'page-explorar',
  templateUrl: 'explorar.html'
})
export class ExplorarPage {
  listaDeFilmes$:Observable<any>;
  listaDeFilmesPesquisa$:Observable<any>;
  pesquisa:string;

  constructor(
    public navCtrl: NavController,
    public filmesProvider: FilmesProvider) {

  }

  ionViewDidEnter(){
    this.pesquisa = '';
    this.listaDeFilmes$ = this.filmesProvider.getListaDeFilmesGroup();
  }

  onPesquisa(evento){
    if(this.pesquisa == ''){
      this.listaDeFilmes$ = this.filmesProvider.getListaDeFilmesGroup();
    }else{
      this.listaDeFilmesPesquisa$ = this.filmesProvider.getListaDeFilmes(this.pesquisa, 100, 1);
    }
  }
}
