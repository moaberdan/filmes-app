import { Component } from '@angular/core';

import { NovoFilmePage } from '../novo-filme/novo-filme';
import { UserPage } from '../user/user';
import { ExplorarPage } from '../explorar/explorar';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ExplorarPage;
  tab2Root = NovoFilmePage;
  tab3Root = UserPage;

  constructor() {

  }
}
