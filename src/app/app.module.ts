import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http'
import { MyApp } from './app.component';
import { PipesModule } from '../pipes/pipes.module';

import { NovoFilmePage } from '../pages/novo-filme/novo-filme';
import { UserPage } from '../pages/user/user';
import { ExplorarPage } from '../pages/explorar/explorar';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FilmesProvider } from '../providers/filmes/filmes';
import { ComponentsModule } from '../components/components.module';
@NgModule({
  declarations: [
    MyApp,
    NovoFilmePage,
    UserPage,
    ExplorarPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    PipesModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    NovoFilmePage,
    UserPage,
    ExplorarPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FilmesProvider
  ]
})
export class AppModule {}
